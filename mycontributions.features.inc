<?php
/**
 * @file
 * mycontributions.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mycontributions_views_api() {
  return array("api" => "3.0");
}
