This module really is a feature module (see https://drupal.org/project/features)
It makes the following actions
a) Display a tab inside user profile to display all resources published by a user (status, post date, edit, delete)
b) Trigger a message to author and editor to notify a new was published and is pending for approval
c) Trigger a message to author to notify his content was published

Note.
To send a message to author I used a token [node:author:mail] provided by Views Bulk Operations module
