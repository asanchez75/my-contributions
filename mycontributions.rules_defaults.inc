<?php
/**
 * @file
 * mycontributions.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function mycontributions_default_rules_configuration() {
  $items = array();
  $items['rules_after_a_content_is_approved'] = entity_import('rules_config', '{ "rules_after_a_content_is_approved" : {
      "LABEL" : "After a content is approved",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_published" : { "node" : [ "node" ] } },
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "news" : "news" } } } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[node:author:mail]",
            "subject" : "[mtnforum.org]  your content have been published!",
            "message" : "Hello [node:author]\\r\\nYour content entitled \\u0022[node:title]\\u0022 was published.\\r\\nIt is available here \\r\\n[node:url]\\r\\nThanks for your patience and collaboration\\r\\nThe mtnforum.org editor\\r\\n",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_for_reviewing_before_publishing'] = entity_import('rules_config', '{ "rules_for_reviewing_before_publishing" : {
      "LABEL" : "For reviewing before publishing",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "workflow" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "5" : "5" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "a.sanchez75@gmail.com,esteli.reyes@condesan.org,musuq.briceno@condesan.org",
            "subject" : "[mtnforum.org]: A new [node:content-type] was published by  [node:author]",
            "message" : "Hello mtnforum editor\\r\\nA new [node:content-type] entitled \\u0022[node:title]\\u0022 was published by  [node:author] on [node:created].\\r\\nTo approve or deny go to [node:edit-url]\\r\\nCheers,\\r\\nThe sysadmin",
            "language" : [ "node:language" ]
          }
        },
        { "mail" : {
            "to" : "[node:author:mail] ",
            "subject" : "[mtnforum.org] Your content is pending for approval",
            "message" : "Hello [node:author]\\r\\nYour content \\r\\n[node:url]\\r\\nwill be reviewed very soon by a content editor.\\r\\nKeep in touch and thanks for your patience\\r\\nThe mtnforum.org editor",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
